# Changelog

## 2.0.2
- Migration de l'instance gitlab d'hébergement du schéma Marchés Publics vers l'instance publique https://gitlab.com/opendatafrance/scdl/marches-publics

## 2.0.1

Changements internes :
- utilisation des [métadonnées standardisées](https://github.com/frictionlessdata/specs/blob/master/specs/patterns.md#table-schema-metadata-properties)

## 2.0.0

- mise à jour de la documentation (titre, description, exemples)
- ajout des champs `MARCHE_UID`, `MODIF_NOTIFICATION_DATE`
- suppression du champ `MODIF_SIGNATURE_DATE`
- modification des valeurs possibles des champs `NATURE_MARCHE`, `PROCEDURE`, `PRIX_FORME`
- ajout de la propriété `updated_at` contenant la date de dernière mise à jour
- transformation de la propriété `example` en `examples` contenant une liste d'exemple de documents

## 1.0.1 (16 mai 2018)

- Utilisation de types et de contraintes mieux adaptés, et simplification de l'expression des contraintes
- Voir aussi https://gitlab.com/opendatafrance/scdl/marches-publics/commit/47373fd44052cd0c106c9fc81c12fc0cc8c38c60

## 1.0 (29 mars 2018)

- Première version. Fonctionne correctement avec les deux fichiers CSV d'exemple.
