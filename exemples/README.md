# Exemples de fichiers CSV portant sur les marchés publics
### D'après l'arrêté du 14 avril 2017 et la spécification d'OpenDataFrance

*Ce répertoire contient des fichiers exemples en format CSV afin de tester le schéma JSON.*

Le premier fichier, `exemple_marche_public.csv` ne retourne pas d'erreurs lorsqu'il est testé.
Le second fichier, `exemple_marche_public_avec_erreurs.csv` a été modifié afin de retourner différentes erreurs.

Ces fichiers ne comportent que des informations fictives et n'ont qu'une valeur de test. Ils sont constitués de 3 lignes. La première est un header comportant les noms des différents champs.

```
MARCHE_ID,  
ACHETEURS_ID,  
ACHETEURS_NOM,  
NATURE_MARCHE,  
MARCHE_OBJET,  
CPV_CODE,  
PROCEDURE,  
LIEU_EXEC_CODE,  
LIEU_EXEC_TYPE,  
LIEU_EXEC_NOM,  
DUREE_MOIS,  
NOTIFICATION_DATE,  
PUBLICATION_DATE,  
MONTANT,  
PRIX_FORME,  
TITULAIRES_ID,  
TITULAIRES_ID_TYPE,  
TITULAIRES_DENOMINATION,  
MODIF_OBJET,  
MODIF_PUBLICATION_DATE,  
MODIF_DUREE_MOIS,  
MODIF_MARCHE_MONTANT,  
MODIF_TITULAIRES_ID,  
MODIF_TITULAIRES_ID_TYPE,  
MODIF_TITULAIRES_DENOMINATION,  
MODIF_SIGNATURE_DATE
```

Les 3 autres lignes décrivent un exemple fictif où un acheteur désigne dans un premier temps deux titulaires. Plus tard une modification du marché intervient et ces deux titulaires sont remplacés par un seul.
