# Marchés publics
Spécification du modèle de données relatif aux marchés publics attribués par une collectivité locale

- nom : `marches-publics`
- page d'accueil : https://gitlab.com/opendatafrance/scdl/marches-publics/
- URL du schéma : https://gitlab.com/opendatafrance/scdl/marches-publics/raw/v2.0.2/schema.json
- version : `2.0.1`
- date de création : 01/03/2018
- date de dernière modification : 28/06/2019
- concerne le pays : FR
- valeurs manquantes représentées par : `[""]`
- contributeurs :
  - Virgile Pesce (auteur)
  - Joël Gombin (contributeur)
  - Charles Nepote (contributeur) [charles.nepote@fing.org](charles.nepote@fing.org)
  - Pierre Dittgen, Jailbreak (contributeur) [pierre.dittgen@jailbreak.paris](pierre.dittgen@jailbreak.paris)
  - Christophe Benz, Jailbreak (contributeur) [christophe.benz@jailbreak.paris](christophe.benz@jailbreak.paris)
- ressources :
  - Exemple de fichier marchés publics valide ([lien](https://gitlab.com/opendatafrance/scdl/marches-publics/raw/v2.0.2/exemples/exemple_marche_public.csv))
  - Exemple de fichier marchés publics avec erreurs ([lien](https://gitlab.com/opendatafrance/scdl/marches-publics/raw/v2.0.2/exemples/exemple_marche_public_avec_erreurs.csv))
- sources :
  - Marchés publics attribués par une collectivité (proposition v1.1) ([lien](https://gitlab.com/opendatafrance/scdl/marches-publics/raw/v2.0.2/referentiels/proposition_specifications_1.1_SCDL_marches_publics.pdf))
  - Arrêté ([lien](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000034492587&dateTexte=&categorieLien=id))
  - Décret ([lien](https://www.legifrance.gouv.fr/eli/decret/2017/5/5/PRMJ1636989D/jo/texte/fr))
  - Article​ sur le blog de l’Etat ([lien](https://www.economie.gouv.fr/daj/ouverture-des-donnees-commande-publique))

## Modèle de données

Ce modèle de données repose sur les 27 champs suivants correspondant aux colonnes du fichier tabulaire.

### `MARCHE_ID`

- titre : Identification du marché public
- description : Exprimé sous la forme d'une chaîne de caractères d'un seul tenant, l'identifiant de marché, composé de trois parties, contient l'année de notification sur 4 caractères, suivie par le numéro d'ordre interne propre à l'acheteur comprenant de 1 à 10 caractères, complété par le numéro d'ordre de la modification sur 2 caractères ('00' si aucune modification, '01' si une première modification intervient, '02' si une deuxième modification intervient, etc).
- type : chaîne de caractères
- exemple : `'2014NNNNNNNNNN00' ou '2014NNNNNNNNNN01'`
- valeur obligatoire
- motif : `^\d{4}.{1,10}\d{2}$`

### `MARCHE_UID`

- titre : Identifiant unique du marché public
- description : Exprimé sous la forme d'une chaîne de caractères d'un seul tenant, cet identifiant unique contient `MARCHE_ID` préfixé de `ACHETEURS_ID`, c'est-à-dire du [numéro SIRET](https://fr.wikipedia.org/wiki/Syst%C3%A8me_d%27identification_du_r%C3%A9pertoire_des_%C3%A9tablissements) de l'acheteur.
- type : chaîne de caractères
- exemple : `'213502388000192014NNNNNNNNNN00'`
- valeur optionnelle
- motif : `^\d{14}\d{4}.{1,10}\d{2}$`

### `ACHETEURS_ID`

- titre : Identifiant de l'acheteur
- description : Identifiant du [Système d'Identification du Répertoire des Etablissements](https://fr.wikipedia.org/wiki/Syst%C3%A8me_d%27identification_du_r%C3%A9pertoire_des_%C3%A9tablissements) (SIRET) de l'acheteur, composé de 9 chiffres SIREN + 5 chiffres NIC d’un seul tenant.
- type : chaîne de caractères
- exemple : `21350238800019`
- valeur obligatoire
- motif : `^\d{14}$`

### `ACHETEURS_NOM`

- titre : Nom de l'acheteur
- description : Nom officiel de la collectivité qui passe le marché. Si le marché est passé par un groupement d'acheteurs, le nom désigne le mandataire du groupement.
- type : chaîne de caractères
- exemple : `Ville de Rennes`
- valeur obligatoire
- motif : `[A-Za-zÀÂÄÇÉÈÊËÎÏÔÖÙÛÜŸàâäçéèêëîïôöùûüÿÆŒæœ \-']*`

### `NATURE_MARCHE`

- titre : Nature du marché
- description : La nature du marché est désignée en choisissant une valeur parmi une liste pré-établie de valeurs possibles : 'Marché', 'Marché de partenariat', 'Accord-cadre', ou 'Marché subséquent'.
- type : chaîne de caractères
- exemple : `Accord-cadre`
- valeur obligatoire
- valeurs autorisées : `["Marché","Marché de partenariat","Accord-cadre","Marché subséquent"]`

### `MARCHE_OBJET`

- titre : Objet du marché ou du lot
- description : Description synthétique de l'objet du marché ou du lot qui ne doit pas excéder 256 caractères.
- type : chaîne de caractères
- exemple : `Entretien des jardins municipaux`
- valeur obligatoire
- taille maximale : 256

### `CPV_CODE`

- titre : Code CPV principal
- description : Le CPV (Common Procurement Vocabulary) est un vocabulaire commun qui désigne un [système de classification pour les marchés publics](https://simap.ted.europa.eu/fr/web/simap/cpv) de l'Union Européenne, rendu obligatoire par le [règlement (CE) nº 213/2008](https://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=OJ:L:2008:074:0001:0375:FR:PDF). Le vocabulaire principal du CPV repose sur une structure arborescente de codes comptant jusqu'à 9 chiffres (un code à 8 chiffres complété par un chiffre de contrôle séparés par un tiret du milieu) auxquels, pour chacun d'entre eux, correspond un intitulé qui décrit le type de fournitures, de travaux ou de services, objet du marché. Même s'il est toléré, il préférable d'omettre le chiffre de contrôle et donc de renseigner un code CPV principal sur 8 chiffres.
- type : chaîne de caractères
- exemple : `77313000`
- valeur obligatoire
- motif : `^[0-9]{8}(\-[0-9])?$`

### `PROCEDURE`

- titre : Procédure de passation du marché
- description : Ce champ permet de renseigner la procédure de passation de marché utilisée par l'acheteur en choisissant une valeur parmi une liste pré-établie de valeurs possibles : 'Procédure adaptée', 'Appel d’offres ouvert', 'Appel d’offres restreint', 'Procédure concurrentielle avec négociation', 'Procédure négociée avec mise en concurrence préalable', 'Marché négocié sans publicité ni mise en concurrence préalable', ou 'Dialogue compétitif'.
- type : chaîne de caractères
- exemple : `Marché négocié sans publicité ni mise en concurrence préalable`
- valeur obligatoire
- valeurs autorisées : `["Procédure adaptée","Appel d'offres ouvert","Appel d'offres restreint","Procédure concurrentielle avec négociation","Procédure négociée avec mise en concurrence préalable","Marché négocié sans publicité ni mise en concurrence préalable","Dialogue compétitif"]`

### `LIEU_EXEC_CODE`

- titre : Code du lieu d'exécution
- description : Le code du lieu d'exécution du marché peut être soit le code postal, soit un des codes référencés par l'INSEE dans le [Code Géographique Officiel](https://www.insee.fr/fr/information/2016807) (COG) pour désigner une commune, un canton, un arrondissement, un département, une région, ou un pays. Les codes INSEE sont à privilégier aux dépens du code postal.
- type : chaîne de caractères
- exemple : `35238`
- valeur obligatoire

### `LIEU_EXEC_TYPE`

- titre : Type de code du lieu d'exécution
- description : Ce champ permet de renseigner le type de code utilisé pour désigner le lieu d'exécution du marché en choisissant une valeur parmi une liste pré-établie de valeurs possibles : 'Code postal', 'Code commune', 'Code arrondissement', 'Code canton', 'Code département', 'Code région', 'Code pays'. Hormis le code postal géré par le service national de l'adresse de La Poste, tous ces types de code correspondent à des [codes géographiques](https://www.insee.fr/fr/information/2016807) gérés par l'INSEE.
- type : chaîne de caractères
- exemple : `Code commune`
- valeur obligatoire
- valeurs autorisées : `["Code postal","Code commune","Code arrondissement","Code canton","Code département","Code région","Code pays"]`

### `LIEU_EXEC_NOM`

- titre : Nom du lieu d'exécution
- description : Ce nom désigne le lieu d'exécution du marché et respecte le formalisme de la [chaîne de caractères type](http://xml.insee.fr/schema/commun.html#ChaineFrancaisOfficiel_stype) utilisée par l'INSEE pour décrire les caractères autorisés dans les documents officiels en français.
- type : chaîne de caractères
- exemple : `Rennes`
- valeur obligatoire
- motif : `^[A-Za-zÀÂÄÇÉÈÊËÎÏÔÖÙÛÜŸàâäçéèêëîïôöùûüÿÆŒæœ \-']*$`

### `DUREE_MOIS`

- titre : Durée initiale du marché
- description : La durée initiale du marché est exprimée en nombre de mois telle qu'elle est définie au moment de la publication des données essentielles. Elle comprend la durée des tranches et reconductions potentielles. Sa valeur ne peut pas être inférieure à '1'. Si la durée ne correspond pas à un nombre exact de mois, elle est arrondie au nombre entier supérieur. Par exemple, la valeur '9' est attendue pour une durée de 9 mois ; la valeur '1' est attendue pour une durée de 2 semaines ; la valeur '2' est attendue pour une durée de 1 mois et 3 semaines. Dans le cas d'une modification de la durée du marché, la valeur de ce champ reste identique et celle du champ `MODIF_DUREE_MOIS` est renseignée.
- type : nombre entier
- exemple : `24`
- valeur obligatoire
- valeur minimale : 1

### `NOTIFICATION_DATE`

- titre : Date de la notification du marché au(x) titulaire(s)
- description : Date à laquelle le marché a été notifié au(x) titulaire(s) au format AAAA-MM-JJ suivant la norme internationale [ISO 8601](https://fr.wikipedia.org/wiki/ISO_8601).
- type : date
- exemple : `2014-08-13`
- valeur obligatoire

### `PUBLICATION_DATE`

- titre : Date de la publication des données essentielles du marché
- description : Date à laquelle les données essentielles du marché décrit ont été publiées pour la première fois au format AAAA-MM-JJ suivant la norme internationale [ISO 8601](https://fr.wikipedia.org/wiki/ISO_8601). Cette date ne nécessite donc pas de mise à jour en cas de modification du marché.
- type : date
- exemple : `2014-08-19`
- valeur obligatoire

### `MONTANT`

- titre : Montant forfaitaire ou estimé maximum HT
- description : Ce montant est exprimé en euros hors-taxe sous la forme d'un nombre décimal dans lequel le séparateur entre la partie entière et la partie décimale du nombre est le point. Dans le cas d'une modification du montant du marché, la valeur de ce champ reste identique et celle du champ `MODIF_MARCHE_MONTANT` est renseignée.
- type : nombre réel
- exemple : `127000.50`
- valeur obligatoire

### `PRIX_FORME`

- titre : Forme du prix
- description : Ce champ permet de renseigner la forme du prix utilisée par l'acheteur en choisissant une valeur parmi une liste pré-établie de valeurs possibles. Cette forme est 'Ferme' quand le prix est fixé pour toute la durée marché. Elle est 'Ferme et actualisable' quand le prix peut évoluer périodiquement selon des conditions prévues dans le contrat initial (par exemple, variation d'indice). Elle est 'Révisable' quand l'acheteur et le titulaire peuvent s'entendre sur une modification du prix après la signature du marché.
- type : chaîne de caractères
- exemple : `Ferme et actualisable`
- valeur obligatoire
- valeurs autorisées : `["Ferme","Ferme et actualisable","Révisable"]`

### `TITULAIRES_ID`

- titre : Identifiant du titulaire du marché
- description : Pour identifier le ou les opérateur(s) économique(s) titulaire(s) du marché, plusieurs types d'identifiants peuvent être utilisés. En fonction du type d'identifiant utilisé (champ `TITULAIRES_ID_TYPE`), ce champ contient la valeur correspondante sous la forme d'une chaine de caractères. S'il existe, il est recommandé de privilégier le [numéro SIRET](https://fr.wikipedia.org/wiki/Syst%C3%A8me_d%27identification_du_r%C3%A9pertoire_des_%C3%A9tablissements). Dans le cas d'une modification du titulaire du marché, la valeur de ce champ reste identique et celle du champ `MODIF_TITULAIRES_ID` est renseignée.
- type : chaîne de caractères
- exemple : `21350238800019`
- valeur obligatoire
- motif : `^[A-Z0-9]{9,}$`

### `TITULAIRES_ID_TYPE`

- titre : Type d'identifiant utilisé pour identifier le titulaire du marché
- description : Pour identifier le ou les opérateur(s) économique(s) titulaire(s) du marché, plusieurs types d'identifiants peuvent être utilisés. Ce champ permet de renseigner le type d'identifiant parmi une liste pré-établie de valeurs possibles : 'SIRET', 'TVA', 'TAHITI', 'RIDET', 'FRWF', 'IREP', ou 'HORS UE'. S'il existe, il est recommandé de privilégier le [numéro SIRET](https://fr.wikipedia.org/wiki/Syst%C3%A8me_d%27identification_du_r%C3%A9pertoire_des_%C3%A9tablissements). Dans le cas d'une modification du titulaire du marché, la valeur de ce champ reste identique et celle du champ `MODIF_TITULAIRES_ID_TYPE` est renseignée.
- type : chaîne de caractères
- valeur obligatoire
- valeurs autorisées : `["SIRET","TVA","TAHITI","RIDET","FRWF","IREP","HORS UE"]`

### `TITULAIRES_DENOMINATION`

- titre : Dénomination sociale du titulaire du marché
- description : Ce champ permet de renseigner le nom du ou des opérateur(s) économique(s) titulaire(s) du marché sous la forme d'une chaîne de caractères. Dans le cas d'une modification du titulaire du marché, la valeur de ce champ reste identique et celle du champ `MODIF_TITULAIRES_DENOMINATION` est renseignée.
- type : chaîne de caractères
- exemple : `Garami SARL`
- valeur obligatoire

### `MODIF_OBJET`

- titre : Objet de la modification
- description : Description de l'objet d'une modification du marché intervenue après la publication de ses données essentielles ne pouvant excéder 256 caractères.
- type : chaîne de caractères
- exemple : `Modification du titulaire du marché. Nouveau titulaire : Rodriguez SAS`
- valeur optionnelle
- taille maximale : 256

### `MODIF_NOTIFICATION_DATE`

- titre : Date de la notification de la modification
- description : Date à laquelle une modification du marché intervenue après la publication de ses données essentielles est notifiée par l'acheteur. Cette date est au format AAAA-MM-JJ suivant la norme internationale [ISO 8601](https://fr.wikipedia.org/wiki/ISO_8601).
- type : date
- exemple : `2014-11-06`
- valeur optionnelle

### `MODIF_PUBLICATION_DATE`

- titre : Date de la republication des données suite à une modification
- description : Date à laquelle les données essentielles sont republiées suite à une modification. Cette date est au format AAAA-MM-JJ suivant la norme internationale [ISO 8601](https://fr.wikipedia.org/wiki/ISO_8601).
- type : date
- exemple : `2015-08-19`
- valeur optionnelle

### `MODIF_DUREE_MOIS`

- titre : Modification de la durée initiale du marché
- description : Dans le cas d'une modification de la durée initiale du marché, la nouvelle durée est exprimée en nombre de mois telle qu'elle est définie au moment de la republication des données essentielles. La manière d'exprimer cette nouvelle durée est identique à celle qui prévaut pour exprimer la durée initiale.
- type : nombre entier
- exemple : `36`
- valeur optionnelle
- valeur minimale : 1

### `MODIF_MARCHE_MONTANT`

- titre : Modification du montant forfaitaire ou estimé maximum HT
- description : Dans le cas d'une modification du montant du marché, le nouveau montant est renseigné tel qu'il est défini au moment de la republication des données essentielles. La manière d'exprimer ce nouveau montant est identique à celle qui prévaut pour exprimer le montant initial.
- type : nombre réel
- exemple : `147000.50`
- valeur optionnelle

### `MODIF_TITULAIRES_ID`

- titre : Identifiant d'un nouveau titulaire du marché
- description : Dans le cas d'une modification de l'opérateur économique titulaire du marché, ce champ contient la valeur, exprimée sous la forme d'une chaîne de caractères, qui correspond au type d'identifiant utilisé pour désigner ce nouveau titulaire. S'il existe, il est recommandé de privilégier le [numéro SIRET](https://fr.wikipedia.org/wiki/Syst%C3%A8me_d%27identification_du_r%C3%A9pertoire_des_%C3%A9tablissements). La manière d'exprimer cet identifiant est identique à celle qui prévaut pour exprimer l'identifiant initial.
- type : chaîne de caractères
- exemple : `80053452100011`
- valeur optionnelle

### `MODIF_TITULAIRES_ID_TYPE`

- titre : Type d'identifiant utilisé pour identifier un nouveau titulaire du marché
- description : Dans le cas d'une modification de l'opérateur économique titulaire du marché, ce champ permet de renseigner le type d'identifiant utilisé pour identifier ce nouveau titulaire parmi une liste pré-établie de valeurs possibles : 'SIRET', 'TVA', 'TAHITI', 'RIDET', 'FRWF', 'IREP', ou 'HORS UE'. S'il existe, il est recommandé de privilégier le [numéro SIRET](https://fr.wikipedia.org/wiki/Syst%C3%A8me_d%27identification_du_r%C3%A9pertoire_des_%C3%A9tablissements). La manière d'exprimer ce type d'identifiant est identique à celle qui prévaut pour exprimer le type d'identifiant initial.
- type : chaîne de caractères
- exemple : `SIRET`
- valeur optionnelle
- valeurs autorisées : `["SIRET","TVA","TAHITI","RIDET","FRWF","IREP","HORS UE"]`

### `MODIF_TITULAIRES_DENOMINATION`

- titre : Dénomination sociale d'un nouveau titulaire du marché
- description : Dans le cas d'une modification de l'opérateur économique titulaire du marché, ce champ permet de renseigner le nom de ce nouveau titulaire sous la forme d'une chaîne de caractères. La manière d'exprimer cette dénomination est identique à celle qui prévaut pour exprimer la dénomination initiale.
- type : chaîne de caractères
- exemple : `Rodriguez SAS`
- valeur optionnelle
