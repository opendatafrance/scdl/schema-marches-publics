# Marchés publics

Spécification du modèle de données relatif aux marchés publics attribués par une collectivité locale

## Contexte

Au plus tard le 1er octobre 2018, les collectivités locales, doivent offrir sur leur profil d'acheteur, un accès libre, direct et complet aux données essentielles des marchés publics répondant à un besoin dont la valeur est égale ou supérieure à 25 000€ HT. La nature et les modalités de diffusion de ces données essentielles ont été fixées par voie réglementaire.

La spécification SCDL du modèle de données relatif aux marchés publics attribués par une collectivité locale est adossée au format réglementaire pour la publication des données essentielles dans la commande publique, mais elle transpose ce format arborescent \(fichier xml ou json\) dans un format tabulaire \(fichier csv\) pour que toutes les collectivités, en particulier celles qui ne sont pas équipées d'outils adaptés, soient néanmoins en capacité de produire, ouvrir et diffuser leurs données essentielles de marché.

Cette spécification a été élaborée à partir des sources suivantes :

* **Documents de cadrage juridique**

  * [Article 107 du Décret n° 2016-360 du 25 mars 2016 relatif aux marchés publics](https://www.legifrance.gouv.fr/jo_pdf.do?id=JORFTEXT000032295952)
  * [Arrêté du 14 avril 2017 relatif aux données essentielles dans la commande publique](https://www.legifrance.gouv.fr/jo_pdf.do?id=JORFTEXT000034492587)
  * [Arrêté du 14 avril 2017 relatif aux fonctionnalités et exigences minimales des profils d'acheteurs](https://www.legifrance.gouv.fr/jo_pdf.do?id=JORFTEXT000034492557)
  * [Arrêté du 27 juillet 2018 modifiant l'arrêté du 14 avril 2017 relatif aux données essentielles dans la commande publique](https://www.legifrance.gouv.fr/jo_pdf.do?id=JORFTEXT000037282994)

* **Document de cadrage technique**
  * [Format réglementaire pour la publication des données essentielles des marchés publics français sur le dépôt Github de la mission Etalab](https://github.com/etalab/format-commande-publique)

Si nécessaire, elle sera mise à jour, adaptée et consolidée à partir des mêmes sources.

#### `Avertissement !`

Dans cette spécification, la transposition du format réglementaire \(passage d'un format arborescent à un format tabulaire\) implique de prêter une attention toute particulière aux éventuelles incidences que peuvent susciter la présence, pour un même marché public, de plusieurs acheteurs, de plusieurs titulaires, de plusieurs lots ou encore de plusieurs modifications apportées à ce marché alors que ses données essentielles ont déjà été publiées. Dans ce cas, les variations de valeur des champs concernés nécessitent de démultiplier le nombre de lignes du fichier.

## Outils

* [![](https://scdl.opendatafrance.net/docs/assets/validata-logo-horizontal.png)](https://go.validata.fr/table-schema?schema_name=scdl.marches-publics) [Valider un fichier avec Validata](https://go.validata.fr/table-schema?schema_name=scdl.marches-publics)
* [Télécharger un fichier gabarit au format XLSX](https://scdl.opendatafrance.net/docs/templates/marches-publics.xlsx)

## Voir aussi

Pour poser une question, commenter, faire un retour d’usage ou contribuer à l’amélioration du modèle de données, vous pouvez :

* adresser un message à [scdl@opendatafrance.email](mailto:scdl@opendatafrance.email?subject=Marchés%20publics)
* participer au [fil de discussion dédié sur le forum de la mission Etalab](https://forum.etalab.gouv.fr/t/schemas-de-validation-des-donnees-essentielles-des-marches-publics/3141)
* ouvrir un ticket sur le [dépôt GitLab d’OpenDataFrance](https://gitlab.com/opendatafrance/scdl/marches-publics/issues/new) ou sur le [dépôt Github de la mission Etalab](https://github.com/etalab/format-commande-publique/issues/new)
